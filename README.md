# toaster.js
[![travis][travis-image]][travis-url]
[![bitHound Score][bithound-image]][bithound-url]
[![bitHound Dependencies][bithound-dependencies-image]][bithound-dependencies-url]
[![js-standard-style][js-standard-style-image]][js-standard-style-url]

[travis-image]: https://travis-ci.org/mightyzero/toaster.js.svg?branch=master
[travis-url]: https://travis-ci.org/mightyzero/toaster.js
[bithound-image]: https://www.bithound.io/github/mightyzero/toaster.js/badges/score.svg
[bithound-url]: https://www.bithound.io/github/mightyzero/toaster.js
[bithound-dependencies-image]: https://www.bithound.io/github/mightyzero/toaster.js/badges/dependencies.svg
[bithound-dependencies-url]: https://www.bithound.io/github/mightyzero/toaster.js/develop/dependencies/npm
[js-standard-style-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat
[js-standard-style-url]: http://standardjs.com/
